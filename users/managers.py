from django.contrib.auth.base_user import BaseUserManager


class CustomUserManager(BaseUserManager):
    """
        Custom user model where email is the unique identifiers
        for authentication instead of usernames.
    """
    def create_user(self, email, password, **kwargs):
        """create and save a User with the giver email and password"""
        if not email:
            raise ValueError("The email must be set")
        email = self.normalize_email(email)
        user = self.model(email=email, **kwargs)
        user.set_password(password)
        user.save(self._db)
        return user

    def create_superuser(self, email, password, **kwargs):
        """Create and save a super user with the given email and password"""
        kwargs.setdefault('is_staff', True)
        kwargs.setdefault('is_superuser', True)
        kwargs.setdefault('is_active', True)

        if kwargs.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True')
        if kwargs.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True')
        return self.create_user(email, password, **kwargs)
