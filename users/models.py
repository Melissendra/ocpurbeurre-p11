from django.db import models
from django.contrib.auth.models import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.db.models.signals import pre_save
from django.dispatch import receiver
from django.utils import timezone

from .managers import CustomUserManager


class UserProfile(AbstractBaseUser, PermissionsMixin):
    """ Models where we saved all the user's information"""
    first_name = models.CharField(max_length=100, blank=True, null=True)
    last_name = models.CharField(max_length=100, blank=True, null=True)
    username = models.CharField(max_length=255, blank=True, null=False)
    email = models.EmailField('email address', unique=True)
    birthday = models.CharField(max_length=20, blank=True, null=True)
    user_image = models.ImageField(null=True, blank=True, upload_to="user_images")
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    date_joined = models.DateTimeField(default=timezone.now)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = CustomUserManager()

    def __str__(self):
        return self.email


@receiver(pre_save, sender=UserProfile)
def delete_profile_image(sender, instance, **kwargs):
    if instance.pk:
        try:
            old_image = UserProfile.objects.get(pk=instance.pk).user_image
        except UserProfile.DoesNotExist:
            return
        else:
            new_image = instance.user_image
            if old_image and old_image.url != new_image.url:
                old_image.delete(save=False)
