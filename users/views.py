from django.views.generic import CreateView, UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render
from django.urls import reverse_lazy

from .forms import SignupForm, EditProfileForm


class SignUpView(CreateView):
    """ class to display the signup's page"""
    form_class = SignupForm
    success_url = reverse_lazy('login')
    template_name = 'users/signup.html'


class AccountView(LoginRequiredMixin, CreateView):
    template_name = 'users/account.html'

    def get(self, request, *args, **kwargs):
        fullname = ' '.join([request.user.first_name, request.user.last_name])
        context = {
            'fullname': fullname,
            'username': request.user.username,
            'email': request.user.email,
                   }
        return render(request, self.template_name, context)


class UserEditView(UpdateView, LoginRequiredMixin):
    form_class = EditProfileForm
    template_name = "users/edit_profile.html"
    success_url = reverse_lazy('home')

    def get_object(self):
        return self.request.user
