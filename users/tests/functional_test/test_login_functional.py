from django.conf import settings
from selenium import webdriver
from django.contrib.staticfiles.testing import StaticLiveServerTestCase


class SeleniumTestCustom(StaticLiveServerTestCase):
    """
    Custom class for selenium test
    ' Call super().setUp or super().tearDown() before changing the method
    """
    fixtures = ["test_data.json"]

    def setUp(self):
        """
        Before each selenium test, prepare the webdriver

        :return: void
        """
        # set the arguments for Chrome working well
        options = webdriver.ChromeOptions()
        options.add_argument("--start-maximized")
        options.add_argument("--disable-infobars")
        options.add_argument("--disable-extension")
        options.add_argument("no-sandbox")
        options.add_argument("--disable-dev-shm-usage")
        options.add_argument("--headless")

        # create the chrome driver
        self.selenium = webdriver.Chrome(options=options)

    def tearDown(self):
        """
        Close the driver after usage
        :return: void
        """
        self.selenium.close()


class LoginSeleniumTest(SeleniumTestCustom):
    """ Test of the login to the user's point of view"""
    fixtures = ["test_data.json"]

    def setUp(self):
        super().setUp()

    def tearDown(self):
        self.selenium.close()

    def testLogin(self):
        self.selenium.get('{}{}'.format(self.live_server_url, '/users/login/'))

        username_input = self.selenium.find_element_by_name("username")
        username_input.send_keys("melissendra77@gmail.com")
        password_input = self.selenium.find_element_by_name('password')
        password_input.send_keys("Gregory2310")
        self.selenium.find_element_by_xpath('//button[@type="submit"]').click()
