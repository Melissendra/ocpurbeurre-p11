from django.test import TestCase
from django.contrib.auth import get_user
from django.urls import resolve
from django.contrib.auth.decorators import login_required

from ..views import UserEditView


class TestChangeProfileView(TestCase):
    """ Class to test the profile's update """
    fixtures = ['products/fixtures/test_data.json']

    def test_change_profile_view(self):
        view = resolve('/users/edit-profile/')
        self.assertEqual(
            view.func.__name__, UserEditView.as_view().__name__
        )

    def test_change_profile_status_code(self):
        self.client.login(
            username='melissendra77@gmail.com',
            password="Opales0451"
        )

        response = self.client.get("/users/edit-profile/")
        self.assertEqual(response.status_code, 200)

    def test_change_ok(self):
        self.client.login(
            username='melissendra77@gmail.com',
            password="Opales0451"
        )
        response = self.client.post("/users/edit-profile/", {
            'last_name': '',
            'first_name': 'Béa',
            'username': '',
            'email': 'melissendra77@gmail.com',
            'birthday': '',
        })
        self.assertRedirects(response, '/')
        self.assertEqual(get_user(self.client).first_name, "Béa")
