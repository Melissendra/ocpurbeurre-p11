from . import *

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'testdb',
        'USER': 'postgres',
        'PASSWORD': 'test123',
        'HOST': 'postgres',
        'PORT': '5432'
    },
}
